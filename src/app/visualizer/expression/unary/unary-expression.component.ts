import { Component, AfterViewInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Injector, Input } from '@angular/core';
import { VisualizerService } from '../../../services/VisualizerService';

@Component({
  selector: 'app-unary-expression',
  templateUrl: './unary-expression.component.html',
  styleUrls: ['./unary-expression.component.css']
})
export class UnaryExpressionComponent implements AfterViewInit {
  // private operator: string = "-";
  // private right: any;
  private visualizerService: VisualizerService;

  @Input()
  operator: string;

  @Input()
  right: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    this.visualizerService = new VisualizerService();
  }

  ngAfterViewInit() {
    // const resolvedRightComponent = this.componentFactoryResolver.resolveComponentFactory(this.rightComponent);
    // this.parent.createComponent(resolvedRightComponent);
  }
}
