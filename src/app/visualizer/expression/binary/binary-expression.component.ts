import { Component, AfterViewInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { VisualizerService } from '../../../services/VisualizerService';
import { OperatorComponent } from '../../operator/operator.component';

@Component({
  selector: 'app-binary-expression',
  templateUrl: './binary-expression.component.html',
  styleUrls: ['./binary-expression.component.css']
})
export class BinaryExpressionComponent implements AfterViewInit {
  private visualizerService: VisualizerService;
  private operatorValue: string;

  @Input()
  public left: any;

  @Input()
  public operator: any;

  @Input()
  public right: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    this.visualizerService = new VisualizerService();
    this.operatorValue = this.getOperatorByType(this.operator);
  }

  ngAfterViewInit() {
    // const resolvedLeftComponent = this.componentFactoryResolver.resolveComponentFactory(this.leftComponent);
    // this.parent.createComponent(resolvedLeftComponent);
    // const resolvedOperatorComponent = this.componentFactoryResolver.resolveComponentFactory(this.operatorComponent);
    // this.parent.createComponent(resolvedOperatorComponent);
    // const resolvedRightComponent = this.componentFactoryResolver.resolveComponentFactory(this.rightComponent);
    // this.parent.createComponent(resolvedRightComponent);
  }

  private getOperatorByType(type: string): string {
    switch(type) {
        case "ADDITION":
            return "+";
        case "SUBTRACTION":
            return "-";
        case "MULTIPLICATION":
                return "*";
        case "DIVISION":
            return "/";
    }
}
}
