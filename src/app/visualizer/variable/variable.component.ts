import { Component, AfterViewInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { VisualizerService } from '../../services/VisualizerService';

@Component({
  selector: 'app-variable',
  templateUrl: './variable.component.html',
  styleUrls: ['./variable.component.css']
})
export class VariableComponent implements AfterViewInit {
  private visualizerService: VisualizerService;
  private dataTypes: string[] = ["NUMBER", "STRING", "DATE"];
  private selectedDataType: string;

  @Input()
  dataType: string;

  @Input()
  name: string;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    this.visualizerService = new VisualizerService();
    this.selectedDataType = this.dataTypes[this.dataType];

  }

  ngAfterViewInit() {
    // const resolvedValueComponent = this.componentFactoryResolver.resolveComponentFactory(this.valueComponent);
    // this.parent.createComponent(resolvedValueComponent);
  }
}
