import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, AfterViewInit, Input } from '@angular/core';
import { BinaryExpressionComponent } from '../expression/binary/binary-expression.component';
import { VisualizerService } from '../../services/VisualizerService';

@Component({
  selector: 'app-expression-container',
  templateUrl: './expression-container.component.html',
  styleUrls: ['./expression-container.component.css']
})
export class ExpressionContainerComponent implements AfterViewInit {
  @Input()
  public tree: any;

  private componentType: string;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    if(this.tree){
      this.componentType = this.getNodeType(this.tree.type);
    }
  }

  ngAfterViewInit() {
    // this.expressionComponent = this.componentFactoryResolver.resolveComponentFactory(this.component);
    // this.parent.createComponent(this.expressionComponent);
  }

  private getNodeType(tree): any {
    const nodeType = tree.type;
    if(nodeType === "ADDITION" || nodeType === "SUBTRACTION" || nodeType === "MULTIPLICATION" || nodeType === "DIVISION") {
        return "BinaryExpressionComponent";
    } else if (nodeType === "NEGATION") {
        return "UnaryExpressionComponent";
    } else if (nodeType === "VARIABLE") {
        return "VariableComponent";
    } else if (nodeType === "FUNCTION") {
        return "FunctionComponent";
    } else if (nodeType === "NUMBER" || nodeType === "PI") {
        return "ValueComponent";
    }
  }
}
