import { Component, AfterViewInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input,  } from '@angular/core';
import { VisualizerService } from '../../services/VisualizerService';

@Component({
  selector: 'app-function',
  templateUrl: './function.component.html',
  styleUrls: ['./function.component.css']
})
export class FunctionComponent implements AfterViewInit {

  private visualizerService: VisualizerService;
  private functionNames: string[] = ["absolute value of", "cosine"];

  @Input()
  name: string;

  @Input()
  parameter: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    this.visualizerService = new VisualizerService();
  }

  ngAfterViewInit() {

  }
}
