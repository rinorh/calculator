import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-operator',
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.css']
})
export class OperatorComponent {
  private operators: string[] = ["+", "-", "*", "/"];
  private selectedOperator: string;
  
  @Input()
  operator: string;

  constructor() {
    this.selectedOperator = this.operators[this.operator];
  }
}
