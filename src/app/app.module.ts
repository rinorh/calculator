import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ValueComponent } from './visualizer/value/value.component';
import { VariableComponent } from './visualizer/variable/variable.component';
import { OperatorComponent } from './visualizer/operator/operator.component';
import { FunctionComponent } from './visualizer/function/function.component';
import { BinaryExpressionComponent } from './visualizer/expression/binary/binary-expression.component';
import { UnaryExpressionComponent } from './visualizer/expression/unary/unary-expression.component';
import { ExpressionContainerComponent } from "./visualizer/expression-container/expression-container.component";
import { VisualizerService } from './services/VisualizerService';

@NgModule({
  declarations: [
    AppComponent,
    ValueComponent,
    VariableComponent,
    OperatorComponent,
    FunctionComponent,
    BinaryExpressionComponent,
    UnaryExpressionComponent,
    ExpressionContainerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    VisualizerService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ExpressionContainerComponent,
    BinaryExpressionComponent,
    UnaryExpressionComponent,
    FunctionComponent,
    VariableComponent,
    ValueComponent,
    OperatorComponent,
  ],
})
export class AppModule { }
