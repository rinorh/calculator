export class VisualizerService {

    public getNodeType(tree): any {
        const nodeType = tree.type;
        if(nodeType === "ADDITION" || nodeType === "SUBTRACTION" || nodeType === "MULTIPLICATION" || nodeType === "DIVISION") {
            return "BinaryExpressionComponent";
            //return new BinaryExpressionComponent(tree.left, new OperatorComponent(this.getOperatorByType(nodeType)), tree.right, this.componentFactoryResolver);
        } else if (nodeType === "NEGATION") {
            return "UnaryExpressionComponent";
            //return new UnaryExpressionComponent("-", tree.expression, this.componentFactoryResolver, this);
        } else if (nodeType === "VARIABLE") {
            return "VariableComponent";
            //return new VariableComponent("NUMBER", tree.name, this.componentFactoryResolver, this);
        } else if (nodeType === "FUNCTION") {
            return "FunctionComponent";
            //return new FunctionComponent(tree.name, tree.arguments[0], this.componentFactoryResolver, this);
        } else if (nodeType === "NUMBER" || nodeType === "PI") {
            return "ValueComponent";
            //return new ValueComponent(tree.value);
        }
    }

    // public setInputs(componentType: string, tree: any): any {
    //     let component: any;
    //     if(componentType === "BinaryExpressionComponent") {
    //         component = BinaryExpressionComponent;
    //         component.left = tree.left;
    //         component.operator = this.getOperatorByType(tree.type);
    //         component.right = tree.right;
    //     } else if (componentType === "UnaryExpressionComponent") {
    //         component = UnaryExpressionComponent;
    //         component.operator = "-";
    //         component.right = tree.expression;
    //     } else if (componentType === "VariableComponent") {
    //         component = VariableComponent;
    //         component.dataType = "NUMBER";
    //         component.name = tree.name;
    //     } else if (componentType === "FunctionComponent") {
    //         component = FunctionComponent;
    //         component.name = tree.name;
    //         component.parameter = tree.arguments[0];
    //     } else if (componentType === "ValueComponent") {
    //         component = ValueComponent;
    //         component.value = tree.value;
    //     }
    //     return component;
    // }

    private getOperatorByType(type: string): string {
        switch(type) {
            case "ADDITION":
                return "+";
            case "SUBTRACTION":
                return "-";
            case "MULTIPLICATION":
                    return "*";
            case "DIVISION":
                return "/";
        }
    }
}